﻿public class Block  implements Comparable<Block>{

	enum Size {
		small, tall, medium, large, wide
	};

	enum Color {
		black, white, blue, green, yellow, red
	};

	enum Shape {
		box, pyramid, rectangle, square, ball
	};

	public char ID;
	public Size size;
	public Color color;
	public Shape shape;

	public Block(char ID) {
		this.ID = ID;
		switch (ID) {
		case 'a':
			shape = Shape.rectangle;
			size = Size.tall;
			color = Color.blue;
			break;
		case 'b':
			shape = Shape.ball;
			size = Size.small;
			color = Color.white;
			break;
		case 'c':
			shape = Shape.square;
			size = Size.large;
			color = Color.red;
			break;
		case 'd':
			shape = Shape.pyramid;
			size = Size.large;
			color = Color.green;
			break;
		case 'e':
			shape = Shape.box;
			size = Size.large;
			color = Color.white;
			break;
		case 'f':
			shape = Shape.rectangle;
			size = Size.wide;
			color = Color.black;
			break;
		case 'g':
			shape = Shape.rectangle;
			size = Size.wide;
			color = Color.blue;
			break;
		case 'h':
			shape = Shape.rectangle;
			size = Size.wide;
			color = Color.red;
			break;
		case 'i':
			shape = Shape.pyramid;
			size = Size.medium;
			color = Color.yellow;
			break;
		case 'j':
			shape = Shape.box;
			size = Size.large;
			color = Color.red;
			break;
		case 'k':
			shape = Shape.ball;
			size = Size.small;
			color = Color.yellow;
			break;
		case 'l':
			shape = Shape.box;
			size = Size.medium;
			color = Color.red;
			break;
		case 'm':
			shape = Shape.ball;
			size = Size.medium;
			color = Color.blue;
			break;
		}
	}


	
	
	public void printBlock() {
		System.out.println(ID+" : "+this.size+" "+this.color+" "+this.shape);
	}
	
	public boolean isContainer() {
		return (this.shape == Shape.box);
	}

	public boolean theSame(String shape, String size, String color) {
		if (!shape.equals("_")) {
			if (!this.shape.toString().equals(shape)) {
				return false;
			}
		}
		if (!color.equals("_")) {
			if (!this.color.toString().equals(color)) {
				return false;
			}
		}
		if (!size.equals("_")) {
			if (!this.size.toString().equals(size)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean theSame(Block block) {
		if(block == null){
			return false;
		}
		return this.ID == block.ID;
	}
	
	public boolean canCarry(Block b){
		//Shape
		if(this.shape == Shape.pyramid || this.shape == Shape.ball ){
			return false;
		}
		
		//Size
		else if(this.shape == Shape.box){
			
			switch (this.size) {
			case small:
				if(b.size == Size.small && b.shape != Shape.box)
				return false;
				else 
					break;
			case medium:
				if(b.size == Size.large || b.size == Size.wide || (b.size == Size.medium && b.shape == Shape.box))
					return false;
				else 
					break;
			case large:
				if(b.size == Size.large && b.shape == Shape.box)
					return false;
				else 
					break;
			default:
				return false;
			}
		}
		else
			switch (this.size) {
			case small:
				if(b.size == Size.large || b.size == Size.medium || b.size == Size.wide)
					return false;
				break;
			case medium:
				if(b.size == Size.large || b.size == Size.wide)
					return false;
			case tall:
				if(b.size == Size.large || b.size == Size.medium || b.size == Size.wide)
					return false;
				break;

			default:
				break;
			}
		
		return true;
	}

	@Override
	public int compareTo(Block block) {
		
		if (block == null || this.canCarry(block)) {
			return 1;
		} else {
			return -1;
		}
	}
}