
/**
 * Keeps track of a specific move
 *
 */
public class MovePoint {

	public int startPile;
	public int endPile;
	public int cost;
	
	public MovePoint(int start, int end, int cost) {
		startPile = start;
		endPile = end;
		this.cost = cost;
	}
	
}
