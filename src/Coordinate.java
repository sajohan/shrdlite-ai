
public class Coordinate {
	public int x;
	public int y;
	
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public boolean theSame(Coordinate compCoord){
		if((compCoord.x == x) && (compCoord.y == y)){
			return true;
		} else {
			return false;
		}
	}
	
	public void printCoordinate(){
		System.out.println("( " + x + " , " + y + " )");
	}
	
	

	@Override
	public boolean equals(Object obj){
		Coordinate compCoord = null;
		if(obj instanceof Coordinate){
			compCoord = (Coordinate)obj;
		}
		if((compCoord.x == x) && (compCoord.y == y)){
			return true;
		} else {
			return false;
		}
		
	}
	

}
