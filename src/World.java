import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class World {
	protected Block[][] contents;
	protected ArrayList<Block> allBlocks;
	protected Block hand;

	public World(String worldRepresentation) {
		final String[] columnRepresentation = worldRepresentation.split(";");
		int rowsQty = 0;
		for (char c : worldRepresentation.toCharArray()) {
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
				++rowsQty;
			}
		}
		rowsQty = 13;
		allBlocks = new ArrayList<Block>();
		contents = new Block[columnRepresentation.length][rowsQty];
		for (int i = 0; i < columnRepresentation.length; ++i) {
			final String column = columnRepresentation[i].trim();
			final String rows[] = column.split(",");
			for (int j = 0; j < rows.length; ++j) {
				rows[j].trim();
				if (rows[j].isEmpty()) {
					continue; // PROCEED!
				}
				contents[i][j] = new Block(rows[j].charAt(0));
				allBlocks.add(contents[i][j]);
			}
		}
	}

	public World(Block[][] goalstate) {
		this.allBlocks = new ArrayList<Block>();
		this.contents = new Block[goalstate.length][goalstate[0].length];
		for (int i = 0; i < goalstate.length; ++i) {
			for (int j = 0; j < goalstate[0].length; ++j) {
				this.contents[i][j] = goalstate[i][j];
				if (this.contents[i][j] != null) {
					this.allBlocks.add(this.contents[i][j]);
				}
			}
		}
	}

	public void printWorld() {
		if (contents == null) {
			return;
		}
		for (int j = contents[0].length - 1; j >= 0; --j) {
			System.out.print("# ");
			for (int i = 0; i < contents.length; ++i) {
				if (contents[i][j] != null) {
					System.out.print(" " + contents[i][j].ID);
				} else {
					System.out.print(" _");
				}
			}
			System.out.println();
		}
		System.out.print("# Hand holds: ");
		if (hand != null) {
			hand.printBlock();
		} else {
			System.out.println("# <nothing>");
		}
	}

	public static void main(String args[]) {

		Scanner stdin = new Scanner(new BufferedInputStream(System.in));
		boolean working = true;
		String command;
		System.out.println("Initial world state:");
		World world = new World("; a,b ; c,d ; ; e,f,g,h,i ; ; ; j,k ; ; l,m");
		world.printWorld();
		World goalWorld;
		while (working) {
			System.out.println("Enter parse tree (or empty line to quit):");
			command = stdin.nextLine();
			if (command.isEmpty()) {
				working = false;
				continue;
			}
			System.out.println("Goal state:");
			goalWorld = world.parseCommand(command);
			if (goalWorld == null) {
				System.out.println("No goalstate found :(");
				break;
			}
			goalWorld.printWorld();
			world = goalWorld;
		}
		stdin.close();
	}

	public Block[] selectBlocks(String argument) {
		return this.selectBlocksFromWorld(argument, new ArrayList<Block>(allBlocks));
	}

	public ArrayList<Coordinate> selectLocations(String argument) {
		return this.selectLocationsFromWorld(argument, new ArrayList<Block>(allBlocks));
	}

	private Block[][] makeEndState(Block[] blocks, ArrayList<Coordinate> coordinateList) {

		Block[][] returnWorld = new Block[contents.length][contents[0].length * 2];
		for (int column = 0; column < contents.length; column++) {
			for (int row = 0; row < contents[0].length; row++) {
				returnWorld[column][row] = contents[column][row];
			}
		}
		
		ArrayList<Block> sortingList = new ArrayList<Block>();
		for(Block block : blocks){
			sortingList.add(block);
		}
		Collections.sort(sortingList);
		blocks = sortingList.toArray(blocks);
		
		
		if (blocks[0] == null) {
			boolean needsMoving = true;
			for (int blockCounter = 1; blockCounter < blocks.length; blockCounter++) {
				Coordinate targetLocation = null;
				for (Coordinate possibleCoord : coordinateList) {
					if (possibleCoord.theSame(new Coordinate(-1, -1))) {
						continue;
					}
					if (blocks[blockCounter].theSame(contents[possibleCoord.x][possibleCoord.y])) {
						needsMoving ^= true;
						break;
					} else if (targetLocation == null) {
						targetLocation = possibleCoord;
					}
				}
				if (needsMoving) {
					moveBlockInWorld(returnWorld, blocks[blockCounter], targetLocation);
					break;
				}
			}
		} else {

			for (int blockCounter = 0; blockCounter < blocks.length; blockCounter++) {
				boolean needsMoving = true;
				for (Coordinate possibleCoord : coordinateList) {
					if (possibleCoord.theSame(new Coordinate(-1, -1))) {
						continue;
					}
					if (blocks[blockCounter].theSame(contents[possibleCoord.x][possibleCoord.y])) {
						needsMoving ^= true;
						break;
					}
				}
				if (needsMoving) {
					boolean moved = false;
					for (Coordinate targetLocation : coordinateList) {
						if (moveBlockInWorld(returnWorld, blocks[blockCounter], targetLocation)) {
							moved = true;
							break;
						}
					}
					if (!moved) {
						return null;
					}

				}
			}
		}

		Block[][] actualReturn = new Block[contents.length][contents[0].length];
		for (int i = 0; i < actualReturn.length; ++i) {
			for (int j = 0; j < actualReturn[0].length; ++j) {
				actualReturn[i][j] = returnWorld[i][j];
			}
		}

		return actualReturn;
	}

	private boolean moveBlockInWorld(Block[][] returnWorld, Block blockToMove, Coordinate targetLocation) {
		
		Block blockAbove = returnWorld[targetLocation.x][targetLocation.y];
		Block blockBelow = null;
		if (targetLocation.y > 0) {
			for ( int i = targetLocation.y-1; i >= 0; --i) {
				if (returnWorld[targetLocation.x][i] != null) {
					blockBelow = returnWorld[targetLocation.x][i];
					break;
				}
			}
		}
		
		if (blockAbove != null) {
			if (!blockToMove.canCarry(blockAbove)){
				return false;
			}
		}
		
		if (blockBelow != null) {
			if (!blockBelow.canCarry(blockToMove)) {
				return false;
			}
		}
		Coordinate oldCoord = getBlockCoordinate(blockToMove, returnWorld);
		returnWorld[oldCoord.x][oldCoord.y] = null;
		for (int i = returnWorld[0].length - 2; i >= targetLocation.y; --i) {
			returnWorld[targetLocation.x][i + 1] = returnWorld[targetLocation.x][i];
		}
		returnWorld[targetLocation.x][targetLocation.y] = blockToMove;
		applyGravity(returnWorld);
		return true;
	}

	private Coordinate getBlockCoordinate(Block blockToMove, Block[][] returnWorld) {
		for (int column = 0; column < returnWorld.length; column++) {
			for (int row = 0; row < returnWorld[0].length; row++) {
				if (returnWorld[column][row] != null) {
					if (returnWorld[column][row].ID == blockToMove.ID) {
						return new Coordinate(column, row);
					}
				}
			}
		}
		return new Coordinate(-1, -1);
	}

	private void applyGravity(Block[][] returnWorld) {
		Coordinate fallCoordinate = new Coordinate(-1, -1);
		for (int column = 0; column < returnWorld.length; column++) {
			for (int row = 1; row < returnWorld[0].length; row++) {
				if (returnWorld[column][row] != null && returnWorld[column][row - 1] == null) {
					for (int fall = row - 1; fall >= 0; fall--) {
						if (returnWorld[column][fall] == null) {
							fallCoordinate.x = column;
							fallCoordinate.y = fall;
						} else {
							break;
						}
					}
					returnWorld[fallCoordinate.x][fallCoordinate.y] = returnWorld[column][row];
					returnWorld[column][row] = null;

				}
			}
		}
	}

	public World parseCommand(String argument) {

		Block[] targetBlocks = null;
		ArrayList<Coordinate> targetCoordinates = null;
		argument = getParanthContent(argument);
		argument = argument.trim();
		String[] argumentParts = argument.split(" ");
		String function = argumentParts[0];
		function = function.trim();
		String data = argument.substring(argument.indexOf(function) + function.length());
		data = data.trim();
		World worldCopy = new World(this.contents);
		worldCopy.hand = this.hand;
		switch (function) {
		case "move":
			if (hand != null) {
				this.dropIt();
			}
			String firstData = getParanthContent(data);
			String secondData = data.substring(data.indexOf(firstData) + firstData.length() + 1);
			firstData = firstData.trim();
			secondData = getParanthContent(secondData);
			secondData = secondData.trim();
			targetBlocks = selectBlocks(firstData);
			targetCoordinates = selectLocations(secondData);
			this.hand = worldCopy.hand;
			this.contents = worldCopy.contents;
			break;
		case "put":
			if (hand != null) {
				targetBlocks = new Block[] { hand };
				this.dropIt();
			} else {
				return null;
			}
			data = getParanthContent(data).trim();
			targetCoordinates = selectLocations(data);
			World returnWorld = new World(makeEndState(targetBlocks, targetCoordinates));
			returnWorld.hand = null;
			this.hand = worldCopy.hand;
			this.contents = worldCopy.contents;
			return returnWorld;
		case "take":
			World copyWorld = new World(contents);
			copyWorld.hand = this.hand;
			if (hand != null) {
				copyWorld.dropIt();
			}
			data = getParanthContent(data);
			data = data.trim();
			targetBlocks = selectBlocks(data);
			if (targetBlocks.length == 0 || (targetBlocks.length > 1 && targetBlocks[0] != null)) {
				return null;
			}
			int index;
			if (targetBlocks.length > 1) {
				index = 1;
			} else {
				index = 0;
			}
			Coordinate coords = getBlockCoordinate(targetBlocks[index], copyWorld.contents);
			copyWorld.hand = copyWorld.contents[coords.x][coords.y];
			copyWorld.contents[coords.x][coords.y] = null;
			applyGravity(copyWorld.contents);
			return copyWorld;
		}

		if (targetBlocks == null || targetBlocks.length == 0 || (targetBlocks.length == 1 && targetBlocks[0] == null) || targetCoordinates.size() == 0 || (targetCoordinates.size() == 1 && targetCoordinates.get(0).theSame(new Coordinate(-1, -1)))) {
			return null;
		}
		
		Block[][] newState = makeEndState(targetBlocks, targetCoordinates);
		if (newState == null) {
			return null;
		}
		
		World returnWorld = new World(newState);
		if (worldCopy.hand != null){
			returnWorld.hand = worldCopy.hand;
		}
		return returnWorld;
	}

	private void dropIt() {

		contents[0][contents[0].length - 1] = hand;
		applyGravity(contents);
		this.hand = null;
	}

	// ( move ( the ( block square _ red ) ) ( inside ( the ( block box _ red )
	// ) ) )
	private Block[] selectBlocksFromWorld(String argument, ArrayList<Block> allBlocks) {
		final ArrayList<Block> result = new ArrayList<Block>();
		Block[] resultArray;
		String function = argument.substring(0, argument.indexOf(" "));
		String data = argument.substring(argument.indexOf(" "));
		data = data.trim();
		function = function.trim();

		if (data.length() != 0 && data.equals("floor")) {
			if (function.equals("above")) {
				for (int column = 0; column < contents.length; column++) {
					if (contents[column][1] != null) {
						for (int row = 1; row < contents[0].length; row++) {
							if (contents[column][row] != null) {
								result.add(contents[column][row]);
							}
						}
					}
				}
				resultArray = new Block[result.size()];
				return result.toArray(resultArray);
			} else if (function.equals("ontop")) {
				for (int column = 0; column < contents.length; column++) {
					if (contents[column][0] != null) {
						result.add(contents[column][0]);
					}
				}
				resultArray = new Block[result.size()];
				return result.toArray(resultArray);

			} else {
				resultArray = new Block[result.size()];
				return result.toArray(resultArray);
			}

		}

		switch (function) {
		case "the":
			data = getParanthContent(data);
			data = data.trim();
			Block[] resultFromThe = selectBlocksFromWorld(data, allBlocks);
			return resultFromThe;
		case "block":
			data = data.trim();
			String[] dataParts = data.split(" ");
			for (int i = 0; i < dataParts.length; ++i) {
				dataParts[i].trim();
			}
			for (Block block : allBlocks) {
				if (block.theSame(dataParts[0], dataParts[1], dataParts[2])) {
					result.add(block);
				}
			}
			break;
		case "all":
			data = getParanthContent(data);
			data = data.trim();
			Block[] resultFromAll = selectBlocksFromWorld(data, allBlocks);
			if (resultFromAll.length > 0 && resultFromAll[0] == null) {
				Block[] actualResult = new Block[resultFromAll.length - 1];
				System.arraycopy(resultFromAll, 1, actualResult, 0, actualResult.length);
				return actualResult;
			} else {
				return resultFromAll;
			}
		case "any":
			data = getParanthContent(data);
			data = data.trim();

			Block[] resultFromAny = selectBlocksFromWorld(data, allBlocks);
			if (resultFromAny.length > 0 && resultFromAny[0] == null) {
				return resultFromAny;
			}
			if (resultFromAny.length >= 1) {
				Block[] anyResult = new Block[resultFromAny.length + 1];
				System.arraycopy(resultFromAny, 0, anyResult, 1, resultFromAny.length);
				return anyResult;
			} else {
				return new Block[0];
			}
		case "thatis":
			String firstData = getParanthContent(data);
			String secondData = data.substring(data.indexOf(firstData) + firstData.length() + 1);
			firstData = firstData.trim();
			secondData = getParanthContent(secondData);
			secondData = secondData.trim();
			Block[] firstSet = selectBlocksFromWorld(firstData, allBlocks);
			Block[] secondSet = selectBlocksFromWorld(secondData, allBlocks);
			int startofSecon = 0;
			if (secondSet.length > 0 && secondSet[0] == null) {
				startofSecon = 1;
			}
			for (int i = 0; i < firstSet.length; ++i) {
				for (int j = startofSecon; j < secondSet.length; ++j) {
					if (firstSet[i].theSame(secondSet[j])) {
						result.add(firstSet[i]);
					}
				}
			}
			if (secondSet.length > 0 && secondSet[0] == null) {
				Block[] thatIsResult = new Block[result.size() + 1];
				resultArray = new Block[result.size()];
				resultArray = result.toArray(resultArray);
				System.arraycopy(resultArray, 0, thatIsResult, 1, result.size());
				return thatIsResult;
			}
			resultArray = new Block[result.size()];
			return result.toArray(resultArray);
		case "leftof":
			data = getParanthContent(data);
			data = data.trim();
			Block[] leftofData = selectBlocksFromWorld(data, allBlocks);
			int leftmostCol = contents.length;
			if (leftofData.length > 0 && leftofData[0] == null) {
				leftmostCol = -1;
				for (int k = 1; k < leftofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(leftofData[k])) {
									if (i > leftmostCol)
										leftmostCol = i;
								}
							}
						}
					}
				}
			} else {

				for (int k = 0; k < leftofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(leftofData[k])) {
									if (i < leftmostCol)
										leftmostCol = i;
								}
							}
						}
					}
				}
			}
			if (leftofData.length > 0 && leftofData[0] == null) {
				result.add(null);
			}
			for (int i = 0; i < leftmostCol; ++i) {
				for (int k = 0; k < contents[0].length; ++k) {
					if (contents[i][k] != null) {
						result.add(contents[i][k]);
					}
				}
			}
			resultArray = new Block[result.size()];
			return result.toArray(resultArray);
		case "rightof":
			data = getParanthContent(data);
			data = data.trim();
			Block[] rightofData = selectBlocksFromWorld(data, allBlocks);
			int rightmost = -1;
			if (rightofData.length > 0 && rightofData[0] == null) {
				rightmost = contents.length;
				for (int k = 1; k < rightofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(rightofData[k])) {
									if (i < rightmost)
										rightmost = i;
								}
							}
						}
					}
				}
			} else {
				for (int k = 0; k < rightofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(rightofData[k])) {
									if (i > rightmost)
										rightmost = i;
								}
							}
						}
					}
				}
			}

			if (rightofData.length > 0 && rightofData[0] == null) {
				result.add(null);
			}
			for (int i = rightmost + 1; i < contents.length; ++i) {
				for (int k = 0; k < contents[0].length; ++k) {
					if (contents[i][k] != null) {
						result.add(contents[i][k]);
					}
				}
			}
			resultArray = new Block[result.size()];
			return result.toArray(resultArray);
		case "above":
			data = getParanthContent(data);
			data = data.trim();
			Block[] aboveData = selectBlocksFromWorld(data, allBlocks);
			if (aboveData.length != 1) {
				if (aboveData.length > 0 && aboveData[0] == null) {
					result.add(null);
					for (int k = 1; k < aboveData.length; ++k) {
						for (int column = 0; column < contents.length; column++) {
							for (int row = 0; row < contents[0].length; row++) {
								if (contents[column][row] != null) {
									if (contents[column][row].theSame(aboveData[k])) {
										for (int i = row + 1; i < contents[0].length; i++) {
											if (contents[column][i] != null) {
												if (!result.contains(contents[column][i])) {
													result.add(contents[column][i]);
												}
											}
										}
									}
								}
							}
						}
					}
					resultArray = new Block[result.size()];
					return result.toArray(resultArray);
				} else {
					return new Block[0];
				}
			} else {
				for (int column = 0; column < contents.length; column++) {
					for (int row = 0; row < contents[0].length; row++) {
						if (contents[column][row] != null) {
							if (contents[column][row].theSame(aboveData[0])) {
								for (int i = row + 1; i < contents[0].length; i++) {
									if (contents[column][i] != null) {
										result.add(contents[column][i]);
									} else {
										resultArray = new Block[result.size()];
										return result.toArray(resultArray);
									}
								}
							}
						}
					}
				}
			}
		case "ontop":
			data = getParanthContent(data);
			data = data.trim();
			Block[] ontopData = selectBlocksFromWorld(data, allBlocks);
			if (ontopData.length != 1) {
				if (ontopData.length > 0 && ontopData[0] == null) {
					// result.add(null);
					for (int k = 1; k < ontopData.length; ++k) {
						Coordinate blockCoord = getBlockCoordinate(ontopData[k]);
						blockCoord.y += 1;
						Block block = contents[blockCoord.x][blockCoord.y];
						if (block != null) {
							result.add(block);
						}
					}
					resultArray = new Block[result.size()];
					return result.toArray(resultArray);
				} else {
					return new Block[0];
				}
			} else {
				Coordinate blockCoord = getBlockCoordinate(ontopData[0]);
				blockCoord.y += 1;
				Block block = contents[blockCoord.x][blockCoord.y];
				if (block != null) {
					result.add(block);
					resultArray = new Block[result.size()];
					return result.toArray(resultArray);
				} else {
					return new Block[0];
				}

			}
		case "under":
			data = getParanthContent(data);
			data = data.trim();
			Block[] belowData = selectBlocksFromWorld(data, allBlocks);
			if (belowData.length != 1) {
				if (belowData.length > 0 && belowData[0] == null) {
					result.add(null);
					for (int k = 1; k < belowData.length; ++k) {
						for (int column = 0; column < contents.length; column++) {
							for (int row = 0; row < contents[0].length; row++) {
								if (contents[column][row] != null) {
									if (contents[column][row].theSame(belowData[k])) {
										if (row != 0) {
											if (!result.contains(contents[column][row - 1])) {
												result.add(contents[column][row - 1]);
											}
										}
									}
								}
							}
						}
					}
					resultArray = new Block[result.size()];
					return result.toArray(resultArray);
				} else {
					return new Block[0];
				}
			} else {
				for (int column = 0; column < contents.length; column++) {
					for (int row = 0; row < contents[0].length; row++) {
						if (contents[column][row] != null) {
							if (contents[column][row].theSame(belowData[0])) {
								if (row == 0) {
									return new Block[0];
								} else {
									result.add(contents[column][row - 1]);
									resultArray = new Block[result.size()];
									return result.toArray(resultArray);
								}
							}
						}
					}
				}
			}
		case "inside":
			data = getParanthContent(data);
			data = data.trim();
			Block[] insideData = selectBlocksFromWorld(data, allBlocks);
			if (insideData.length > 0 && insideData[0] == null) {
				result.add(null);
				for (Block block : insideData) {
					if (block != null && block.isContainer()) {
						Coordinate coords = getBlockCoordinate(block);
						Block actualBlock = contents[coords.x][coords.y + 1];
						if (actualBlock != null) {
							result.add(actualBlock);
						}
					}
				}
			} else if (insideData.length == 1) {
				if (insideData[0].isContainer()) {
					Coordinate coords = getBlockCoordinate(insideData[0]);
					Block actualBlock = contents[coords.x][coords.y + 1];
					if (actualBlock != null) {
						result.add(actualBlock);
					}
				}
			}
			resultArray = new Block[result.size()];
			return result.toArray(resultArray);

		case "beside":
			data = getParanthContent(data);
			data = data.trim();
			Block[] besideData = selectBlocksFromWorld(data, allBlocks);
			if (besideData.length != 1) {
				if (besideData.length > 0 && besideData[0] == null) {
					result.add(null);
					for (int k = 1; k < besideData.length; ++k) {
						int req_col = -1;
						for (int j = contents[0].length - 1; j >= 0; --j) {
							for (int i = 0; i < contents.length; ++i) {
								if (contents[i][j] != null) {
									if (besideData[k].theSame(contents[i][j])) {
										req_col = i;
										break;
									}
								}
							}
							if (req_col != -1) {
								break;
							}
						}

						if (req_col > 0) {
							for (int i = 0; i < contents[0].length; ++i) {
								if (contents[req_col - 1][i] != null) {
									if (!result.contains(contents[req_col - 1][i]))
										result.add(contents[req_col - 1][i]);
								}
							}
						}
						if (req_col < contents.length - 1) {
							for (int i = 0; i < contents[0].length; ++i) {
								if (contents[req_col + 1][i] != null) {
									if (!result.contains(contents[req_col + 1][i]))
										result.add(contents[req_col + 1][i]);
								}
							}
						}
					}

					resultArray = new Block[result.size()];
					return result.toArray(resultArray);
				}
			} else {
				int req_col = -1;
				for (int j = contents[0].length - 1; j >= 0; --j) {
					for (int i = 0; i < contents.length; ++i) {
						if (contents[i][j] != null) {
							if (besideData[0].theSame(contents[i][j])) {
								req_col = i;
								break;
							}
						}
					}
					if (req_col != -1) {
						break;
					}
				}

				if (req_col > 0) {
					for (int i = 0; i < contents[0].length; ++i) {
						if (contents[req_col - 1][i] != null) {
							result.add(contents[req_col - 1][i]);
						}
					}
				}
				if (req_col < contents.length - 1) {
					for (int i = 0; i < contents[0].length; ++i) {
						if (contents[req_col + 1][i] != null) {
							result.add(contents[req_col + 1][i]);
						}
					}
				}
				resultArray = new Block[result.size()];
				return result.toArray(resultArray);
			}
		}

		resultArray = new Block[result.size()];
		return result.toArray(resultArray);

	}

	static protected String getParanthContent(String arg) {
		int p_pos = 0;
		int p_neg = 0;
		int i_pos = -1;
		int i_neg = -1;
		for (int i = 0; i < arg.length(); ++i) {
			if (arg.charAt(i) == '(') {
				++p_pos;
				if (p_pos == 1) {
					i_pos = i;
				}
			}
			if (arg.charAt(i) == ')') {
				++p_neg;
			}
			if (p_neg == p_pos && p_pos != 0) {
				i_neg = i;
				break;
			}
		}
		final String result;
		if (i_neg == -1 || i_pos == -1) {
			result = "";
		} else {
			result = arg.substring(i_pos + 1, i_neg);
			result.trim();
		}

		return result;
	}

	public MovePoint analyzeDrop(int endPile, World goalWorld) {

		return analyzeMove(-1, endPile, goalWorld);
	}

	// Move block from startPile to endPile
	public MovePoint analyzeMove(int startPile, int endPile, World goalWorld) {

		Block[][] goalContents = goalWorld.getContents();

		int cost = 0;

		if (startPile == endPile)
			return null;

		// Test if move valid
		for (int y = this.contents[endPile].length - 1; y > 0; y--) {
			if (contents[endPile][y] != null) {
				if (!contents[endPile][y - 1].canCarry(contents[endPile][y])) {
					return null;
				}
				else
					break;
			}
		}
		for (int x = 0; x <= this.contents.length - 1; x++) {
			for (int y = 0; y <= this.contents[x].length - 1; y++) {

				//block is supposed to be on the hand at the end goal
				/*if(goalWorld.getHand() != null){					
					if (goalWorld.getHand().theSame(contents[x][y])) {
						boolean pileCorrect = true;
						//loop through all blocks below the block that is supposed to be on hand at goal
						for (int h = 0; h < y; h++) {
							if(goalContents[x][y] != null){
								if(!goalContents[x][h].theSame(contents[x][h])){
									pileCorrect = false;
									break;
								}					
							}
						}
						if(pileCorrect){
							//continue;
						}
					}
				}*/
				
				// no current block
				if (contents[x][y] == null) {
					// Do nothing
					continue;
				}
				// block but no block in goal
				else if (goalContents[x][y] == null) {
					cost += 2;
					cost += blockCost(x, y, goalContents);
					cost += rightPile(x, contents[x][y], goalContents);

				}
				else if (!contents[x][y].theSame(goalContents[x][y])) {
					cost += 2;
					cost += blockCost(x, y, goalContents);
					cost += rightPile(x, contents[x][y], goalContents);
				}

				if (y == 0 && contents[x][y + 1] == null && goalContents[x][y] == null) {
					cost--;
				}

			}
		}

		return new MovePoint(startPile, endPile, cost);
	}

	public int blockCost(int x, int y, Block[][] goal) {

		if (this.contents[x][y + 1] == null) {
			return 0;
		} else {
			return (2 + blockCost(x, y + 1, goal));
		}

	}

	public int rightPile(int x, Block b, Block[][] goal) {

		for (int y = 0; y <= this.contents[x].length - 1; y++) {
			//System.out.println("YYYYYY:" + y);
			//System.out.println("xxxxxXXX:" + x);
			if (goal[x][y] != null) {
				if (goal[x][y].theSame(b)) {
					return 1;
				}
			}

		}
		return 0;

	}

	public World move(int startPile, int endPile) {
		Block moveIt = null;
		if (startPile != -1) {
			for (int blockHeight = this.contents[startPile].length - 1; blockHeight >= 0; blockHeight--)
			{
				if (this.contents[startPile][blockHeight] != null) {
					moveIt = this.contents[startPile][blockHeight];
					this.contents[startPile][blockHeight] = null;
					break;
				}
			}

		} else {
			moveIt = hand;
			hand = null;
		}
		if(endPile != -1){
			for (int blockHeight = 0; blockHeight <= this.contents[endPile].length - 1; blockHeight++)
			{
				if (this.contents[endPile][blockHeight] == null) {
					this.contents[endPile][blockHeight] = moveIt;
					break;
				}
			}			
		}else{
			hand = moveIt;
		}
		return this;
	}

	public World copyWorld(World originalWorld)
	{
		World newWorld = new World("; a,b ; c,d ; ; e,f,g,h,i ; ; ; j,k ; ; l,m");
		for (int a = 0; a <= originalWorld.contents.length - 1; a++) {
			for (int b = 0; b <= originalWorld.contents[a].length - 1; b++) {
				if (originalWorld.contents[a][b] == null) {
					newWorld.contents[a][b] = null;
				} else {
					newWorld.contents[a][b] = originalWorld.contents[a][b];
				}
			}

		}
		newWorld.hand = originalWorld.getHand();
		return newWorld;
	}

	public Coordinate getBlockCoordinate(Block block) {

		for (int column = 0; column < contents.length; column++) {
			for (int row = 0; row < contents[0].length; row++) {
				if (contents[column][row] != null) {
					if (contents[column][row].ID == block.ID) {
						return new Coordinate(column, row);
					}
				}
			}
		}
		return new Coordinate(-1, -1);
	}

	public ArrayList<Coordinate> selectLocationsFromWorld(String argument, ArrayList<Block> allBlocks) {
		final ArrayList<Coordinate> result = new ArrayList<Coordinate>();
		String function = argument.substring(0, argument.indexOf(" "));
		String data = argument.substring(argument.indexOf(" "));
		data = data.trim();
		function = function.trim();

		if (data.length() != 0 && data.equals("floor")) {
			if (function.equals("above")) {
				for (int column = 0; column < contents.length; column++) {
					if (contents[column][0] != null) {
						for (int row = 0; row < contents[0].length; row++) {
							if (contents[column][row] == null) {
								result.add(new Coordinate(column, row));
							}
						}
					}
				}
				return result;
			} else if (function.equals("ontop")) {
				for (int column = 0; column < contents.length; column++) {
					if (contents[column][0] == null) {
						result.add(new Coordinate(column, 0));
					}
				}
				return result;

			} else {
				return result;
			}

		}

		switch (function) {
		case "the":
			data = getParanthContent(data);
			data = data.trim();
			ArrayList<Coordinate> resultFromThe = selectLocationsFromWorld(data, allBlocks);
			if (resultFromThe.size() >= 1) {
				if (resultFromThe.get(0).theSame(new Coordinate(-1, -1))) {
					return new ArrayList<Coordinate>();
				} else {
					return resultFromThe;
				}
			} else {
				return new ArrayList<Coordinate>();
			}
		case "block":

			data = data.trim();
			String[] dataParts = data.split(" ");
			for (int i = 0; i < dataParts.length; ++i) {
				dataParts[i].trim();
			}
			for (Block block : allBlocks) {
				if (block.theSame(dataParts[0], dataParts[1], dataParts[2])) {
					Coordinate coordAbove = getBlockCoordinate(block);
					coordAbove.y = coordAbove.y + 1;
					result.add(coordAbove);
				}
			}
			break;
		case "all":
			data = getParanthContent(data);
			data = data.trim();
			ArrayList<Coordinate> tempArray = selectLocationsFromWorld(data, allBlocks);
			if (tempArray.size() == 0) {
				return new ArrayList<Coordinate>();
			} else {
				ArrayList<Coordinate> resultFromAll = new ArrayList<Coordinate>();
				resultFromAll.add(new Coordinate(-1, -1));
				resultFromAll.addAll(tempArray);
				return resultFromAll;
			}
		case "any":
			data = getParanthContent(data);
			data = data.trim();
			ArrayList<Coordinate> resultFromAny = selectLocationsFromWorld(data, allBlocks);
			return resultFromAny;
			// if (resultFromAny.size() >= 1) {
			//
			// ArrayList<Coordinate> returnList = new ArrayList<Coordinate>();
			// returnList.add(resultFromAny.get(0));
			// return returnList;
			// } else {
			// return new ArrayList<Coordinate>();
			// }
		case "thatis":
			String firstData = getParanthContent(data);
			String secondData = data.substring(data.indexOf(firstData) + firstData.length() + 1);
			firstData = firstData.trim();
			secondData = getParanthContent(secondData);
			secondData = secondData.trim();
			ArrayList<Coordinate> firstSet = selectLocationsFromWorld(firstData, allBlocks);
			ArrayList<Coordinate> secondSet = selectLocationsFromWorld(secondData, allBlocks);
			if (secondSet.size() >= 1) {
				if (secondSet.get(0).theSame(new Coordinate(-1, -1))) {
					return new ArrayList<Coordinate>();
				}
			}
			for (int i = 0; i < firstSet.size(); ++i) {
				for (int j = 0; j < secondSet.size(); ++j) {
					if (firstSet.get(i).theSame(secondSet.get(j))) {
						result.add(firstSet.get(i));
					}
				}
			}
			return result;
		case "leftof":
			data = getParanthContent(data);
			data = data.trim();
			Block[] leftofData = selectBlocksFromWorld(data, allBlocks);
			if (leftofData.length == 0) {
				return new ArrayList<Coordinate>();
			}
			int leftmostCol = contents.length;
			if (leftofData.length > 1 && leftofData[0] != null) {
				for (int k = 0; k < leftofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(leftofData[k])) {
									if (i < leftmostCol)
										leftmostCol = i;
								}
							}
						}
					}
				}
			} else if (leftofData[0] == null) {
				leftmostCol = -1;
				for (int k = 1; k < leftofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(leftofData[k])) {
									if (i > leftmostCol)
										leftmostCol = i;
								}
							}
						}
					}
				}
			} else {
				leftmostCol = -1;
				for (int k = 0; k < leftofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(leftofData[k])) {
									if (i > leftmostCol)
										leftmostCol = i;
								}
							}
						}
					}
				}
			}

			for (int i = 0; i < leftmostCol; ++i) {
				for (int k = 0; k < contents[0].length; ++k) {
					result.add(new Coordinate(i, k));

				}
			}
			return result;
		case "rightof":
			data = getParanthContent(data);
			data = data.trim();
			Block[] rightofData = selectBlocksFromWorld(data, allBlocks);
			if (rightofData.length == 0) {
				return new ArrayList<Coordinate>();
			}
			int rightmost = -1;
			if (rightofData.length > 1 && rightofData[0] != null) {
				for (int k = 0; k < rightofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(rightofData[k])) {
									if (i > rightmost)
										rightmost = i;
								}
							}
						}
					}
				}
			} else if (rightofData[0] == null) {
				rightmost = contents.length;
				for (int k = 1; k < rightofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(rightofData[k])) {
									if (i < rightmost)
										rightmost = i;
								}
							}
						}
					}
				}
			} else {
				rightmost = contents.length;
				for (int k = 0; k < rightofData.length; ++k) {
					for (int j = contents[0].length - 1; j >= 0; --j) {
						for (int i = 0; i < contents.length; ++i) {
							if (contents[i][j] != null) {
								if (contents[i][j].theSame(rightofData[k])) {
									if (i < rightmost)
										rightmost = i;
								}
							}
						}
					}
				}
			}
			for (int i = rightmost + 1; i < contents.length; ++i) {
				for (int k = 0; k < contents[0].length; ++k) {
					result.add(new Coordinate(i, k));
				}
			}
			return result;
		case "above":
			data = getParanthContent(data);
			data = data.trim();
			Block[] aboveData = selectBlocksFromWorld(data, allBlocks);
			if ((aboveData.length == 0) || (aboveData.length > 1 && aboveData[0] != null)) {
				return new ArrayList<Coordinate>();
			}

			// result.add(aboveData[0]);
			for (Block currentBlock : aboveData) {
				if (currentBlock == null) {
					continue;
				}
				for (int column = 0; column < contents.length; column++) {
					for (int row = 0; row < contents[0].length; row++) {
						if (contents[column][row] != null) {
							if (contents[column][row].theSame(currentBlock)) {
								for (int i = row + 1; i < contents[0].length; i++) {
									if (!result.contains(new Coordinate(column, i))) {
										result.add(new Coordinate(column, i));
									}
								}
							}
						}
					}
				}
			}
			return result;
		case "ontop":
			data = getParanthContent(data);
			data = data.trim();
			Block[] ontopData = selectBlocksFromWorld(data, allBlocks);
			if ((ontopData.length == 0) || (ontopData.length > 1 && ontopData[0] != null)) {
				return new ArrayList<Coordinate>();
			}
			for (Block block : ontopData) {
				if (block == null) {
					continue;
				}
				Coordinate coord = getBlockCoordinate(block);
				coord.y = coord.y + 1;
				if (!result.contains(coord)) {
					result.add(coord);
				}

			}
			return result;

		case "under":
			data = getParanthContent(data);
			data = data.trim();
			Block[] belowData = selectBlocksFromWorld(data, allBlocks);
			if ((belowData.length == 0) || (belowData.length > 1 && belowData[0] != null)) {
				return new ArrayList<Coordinate>();
			}
			for (Block block : belowData) {
				if (block == null) {
					continue;
				}
				for (int column = 0; column < contents.length; column++) {
					for (int row = 0; row < contents[0].length; row++) {
						if (contents[column][row] != null) {
							if (contents[column][row].theSame(block)) {
								for (int i = row; i >= 0; i--) {
									if (!result.contains(new Coordinate(column, i))) {
										result.add(new Coordinate(column, i));
									}
								}
							}
						}
					}
				}
			}
			return result;
		case "inside":
			data = getParanthContent(data);
			data = data.trim();
			Block[] insideData = selectBlocksFromWorld(data, allBlocks);
			if ((insideData.length == 0) || (insideData.length > 1 && insideData[0] != null)) {
				return new ArrayList<Coordinate>();
			}
			for (Block block : insideData) {
				if (block == null) {
					continue;
				}
				if (block.isContainer()) {
					Coordinate coord = getBlockCoordinate(block);
					coord.y = coord.y + 1;
					if (!result.contains(coord)) {
						result.add(coord);
					}
				}
			}

			return result;
		}

		return result;

		// return new ArrayList<Coordinate>();
	}

	public Block[][] getContents() {
		return contents;
	}

	public void setHand(Block hand) {
		this.hand = hand;
	}

	public Block getHand() {
		return hand;
	}

	public boolean compare(World w, Block ignore) {
		for (int a = 0; a < w.contents.length; a++)
		{
			for (int b = 0; b < w.contents[a].length; b++)
			{
				//Don't compare with ignored block
				if(ignore != null && contents[a][b] != null){
					if(this.contents[a][b].theSame(ignore))
						continue;					
				}
			
				if (w.contents[a][b] == null) {
					if (this.contents[a][b] != null) {
						return false;
					}
				}
				else if (this.contents[a][b] == null) {
					if (w.contents[a][b] != null) {
						return false;
					}
				}
				else if (w.contents[a][b].ID != this.contents[a][b].ID)
					return false;
			}
		}
		return true;
	}

}
