import java.util.ArrayList;




public class planner {
	
	public static void main (String args[]) {
        // Read the data
        String holding = args[0];
        String world = args[1];
        String[] trees = args[2].split(";");

        // Print the data
		System.out.println("# Stupid Java planner!");
		System.out.println("# Holding: " + holding);
		System.out.println("# World: " + world);
        for (String t : trees) {
            System.out.println("# Tree: " + t);
        }

        World startState = new World(world);
        System.out.println("# Start world: ");
        try{
        	if(!holding.isEmpty()){
        		startState.setHand(new Block(holding.charAt(0)));
        	}
        	
        }catch(Exception e){
        	for(StackTraceElement elem : e.getStackTrace()){
        		System.out.println(elem.toString());
        	}
        }
        startState.printWorld();
        //Create goal world
        try{   
	        World goalState = null;
	        for(String tree : trees){
	        	goalState = startState.parseCommand(tree);
	        	if(goalState != null){
	        		break;
	        	}        	
	        }
	        if(goalState == null){
	        	System.out.println("Could not find a solution");
	        	System.exit(0);
	        	
	        }
	        System.out.println("# Goal world: ");
	        goalState.printWorld();

    		System.out.println(getPlan(startState, goalState));        	
        }catch(Exception e){
        	System.out.println(e.toString());
        	for(StackTraceElement elem : e.getStackTrace()){
        		System.out.println(elem.toString());
        	}
        }
      
	}
	
	public static String getPlan(World start, World goal){
		
		String plan  = "";
		World currentWorld = start;
		int i = 500;
		
		
		//while(!currentWorld.compare(goal)){
		while(i != 0 && !currentWorld.compare(goal,goal.getHand()) ){
			i--;
		
			ArrayList<MovePoint> moves = new ArrayList<MovePoint>();
			
			//Holding block
			if(currentWorld.getHand() != null){
				for(int testPile = 0 ; testPile <= currentWorld.getContents().length-1 ; testPile++){
					
					//move block and get new world
					World testWorld  = currentWorld.copyWorld(currentWorld);
					testWorld.move(-1, testPile);
					
					MovePoint testMove = testWorld.analyzeDrop( testPile, goal); 	
					if (testMove != null){	
						moves.add(testMove);
					}
					
				}
			}
			//no block in holding
			else{
				//Find the blocks on top of piles
				for(int pile = 0 ; pile <= currentWorld.getContents().length-1 ; pile++){
					for(int height = currentWorld.getContents()[pile].length-1 ; height >= 0 ; height--){
	
						if(currentWorld.getContents()[pile][height] != null){
							
							//Test move block to all other piles
							for(int testPile = 0 ; testPile <= currentWorld.getContents().length-1 ; testPile++){
								
								//move block and get new world
								World testWorld  = currentWorld.copyWorld(currentWorld);
								testWorld.move(pile, testPile);
								
								MovePoint testMove = testWorld.analyzeMove(pile, testPile, goal); 						
								
								if (testMove != null){
									
									moves.add(testMove);
								}
								
							}
							break;
						}
		
					}
			
				 }
			}
			MovePoint smallest = null;
			for(MovePoint moveP : moves){
				if(smallest == null || (moveP.cost < smallest.cost)){
					smallest = moveP;
				}
			}
			System.out.println("# World cost: " + smallest.cost);
			if(smallest.startPile != -1){
				currentWorld = currentWorld.move(smallest.startPile, smallest.endPile);
				plan = plan.concat("pick " +  smallest.startPile + "\n" + "drop " + smallest.endPile + "\n");				
			}else{
				currentWorld = currentWorld.move(smallest.startPile, smallest.endPile);
				plan = plan.concat("drop " + smallest.endPile + "\n");				
				
			}
			//System.out.println(plan);
			//System.out.println("pick " +  largest.startPile + "\n" + "drop " + largest.endPile + "\n");
		}
		
		if(!currentWorld.compare(goal,null)){
			for (int x = 0; x <= currentWorld.contents.length - 1; x++) {
				for (int y = 0; y <= currentWorld.contents[x].length - 1; y++) {
					if(goal.getHand().theSame(currentWorld.contents[x][y])){
						currentWorld.move(x, -1);
						plan = plan.concat("pick " + x );
						break;
					}
				}
			}
			
		}
		System.out.println("# Reached goal state");
	
	
		return plan;
		
		
	}
	
}
